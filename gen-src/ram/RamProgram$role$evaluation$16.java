package ram;
import dexter.lexter.QualifiedToken;
import neverlang.runtime.*;
import neverlang.runtime.dexter.ASTNode;
import ram.VariableAccessor;

public class RamProgram$role$evaluation$16 implements PostorderSemanticAction {
  public void apply(Context $ctx) {
    ASTNode $n = $ctx.node();

			$ctx.nt(16).setValue("value",  $ctx.nt(17).getValue("value"));
			$ctx.addVariable(new VariableAccessor($ctx.nt(17).getValue("value")));
		
  }
}