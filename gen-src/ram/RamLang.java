package ram;
import neverlang.runtime.*;
public class RamLang extends Language {
  public RamLang() {
    importSlices(
     "ram.RamProgram"
    );
    importEndemicSlices(
     "ram.SymbolTableEndemic", 
     "ram.ProgramTableEndemic"
    );
    declare(
     role(Role.Flags.POSTORDER, "preevaluation"), 
     role(Role.Flags.POSTORDER, "debug"), 
     role(Role.Flags.POSTORDER, "evaluation")
    );

  } 
}