package ram;
import neverlang.runtime.Syntax;

public class RamProgram$role$syntax extends Syntax {
 public RamProgram$role$syntax() {
   declareProductions(
    p(nt("Program"), nt("Statement")).setLabel("Prog"),
    p(nt("Statement"), nt("StatementList")),
    p(nt("StatementList"), nt("Statement"), ".", nt("StatementList")),
    p(nt("StatementList"), nt("Statement"), "."),
    p(nt("Statement"), nt("Register"), "+", "1").setLabel("Increment"),
    p(nt("Statement"), nt("Register"), "-.", "1").setLabel("Decrement"),
    p(nt("Statement"), "if", nt("Register"), "=", "0", "goto", nt("Integer")).setLabel("Goto"),
    p(nt("Register"), "R", nt("Integer")).setLabel("Reg"),
    p(nt("Integer"), regex("integer", "[0-9]+")).setLabel("Int")
   );
 }
}