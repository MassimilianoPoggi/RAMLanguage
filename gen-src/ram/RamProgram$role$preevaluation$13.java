package ram;
import dexter.lexter.QualifiedToken;
import neverlang.runtime.*;
import neverlang.runtime.dexter.ASTNode;
import ram.VariableAccessor;

public class RamProgram$role$preevaluation$13 implements PostorderSemanticAction {
  public void apply(Context $ctx) {
    ASTNode $n = $ctx.node();

			$ctx.root().<ProgramTable>getValue("$ProgramTable").put($n.tchild(0).token.row, $n);
		
  }
}