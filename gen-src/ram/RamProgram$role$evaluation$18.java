package ram;
import dexter.lexter.QualifiedToken;
import neverlang.runtime.*;
import neverlang.runtime.dexter.ASTNode;
import ram.VariableAccessor;

public class RamProgram$role$evaluation$18 implements PostorderSemanticAction {
  public void apply(Context $ctx) {
    ASTNode $n = $ctx.node();

			$ctx.nt(18).setValue("value",  $n.tchild(0).token.text);
		
  }
}