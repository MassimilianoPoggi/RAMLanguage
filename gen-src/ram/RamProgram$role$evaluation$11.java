package ram;
import dexter.lexter.QualifiedToken;
import neverlang.runtime.*;
import neverlang.runtime.dexter.ASTNode;
import ram.VariableAccessor;

public class RamProgram$role$evaluation$11 implements PostorderSemanticAction {
  public void apply(Context $ctx) {
    ASTNode $n = $ctx.node();

			int newVal = Math.max(0, $ctx.root().<SymbolTable>getValue("$SymbolTable").get($ctx.nt(12).getValue("value")) - 1);
			$ctx.root().<SymbolTable>getValue("$SymbolTable").put($ctx.nt(12).getValue("value"), newVal);
		
  }
}