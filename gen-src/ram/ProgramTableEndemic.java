package ram;
import neverlang.runtime.*;
public class ProgramTableEndemic extends EndemicSlice {
  public void declarations() {
    declare("ProgramTable",  ram.ProgramTable.getInstance() );

  }
  public void staticDeclarations() {

  }
  public String[] getTags() {
    return new String[] { "" };
  }

}