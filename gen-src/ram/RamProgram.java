package ram;
import neverlang.runtime.*;
public class RamProgram extends Module {
  public RamProgram() {
    declareSyntax(); //null
    declareRole("evaluation", 0, 9, 11, 13, 16, 18);
    declareRole("debug", 9, 11, 13);
    declareRole("preevaluation", 9, 11, 13);

    requireEndemicSlices("ProgramTable", "SymbolTable");
} 
}