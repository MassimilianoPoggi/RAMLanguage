package ram;
import neverlang.runtime.*;
public class SymbolTableEndemic extends EndemicSlice {
  public void declarations() {
    declare("SymbolTable",  ram.SymbolTable.getInstance() );

  }
  public void staticDeclarations() {

  }
  public String[] getTags() {
    return new String[] { "" };
  }

}