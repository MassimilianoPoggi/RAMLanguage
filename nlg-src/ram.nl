module ram.RamProgram {
	imports {
		ram.VariableAccessor;
	}
	reference syntax {
		Prog: Program <-- Statement;

		Statement <-- StatementList;
		StatementList <-- Statement "." StatementList;
		StatementList <-- Statement ".";

		Increment: Statement <-- Register "+" "1";
		Decrement: Statement <-- Register "-." "1";
		Goto: Statement <-- "if" Register "=" "0" "goto" Integer;

		Reg: Register <-- "R" Integer;

		Int: Integer <-- /[0-9]+/{integer};
	}

	role(preevaluation) {
		Increment: .{
			$$ProgramTable.put(#0.row, $Increment[0]);
		}.

		Decrement: .{
			$$ProgramTable.put(#0.row, $Decrement[0]);
		}.

		Goto: .{
			$$ProgramTable.put(#0.row, $Goto[0]);
		}.
	}

	role(debug) {
		Increment: .{
			$Increment[0].isExecutionStep = true;
		}.

		Decrement: .{
			$Decrement[0].isExecutionStep = true;
		}.

		Goto: .{
			$Goto[0].isExecutionStep = true;
		}.
	}

	role(evaluation) {
		Prog: .{
			System.out.println($$SymbolTable.get(0));
		}.

		Increment: .{
			$$SymbolTable.put($Increment[1].value, $$SymbolTable.get($Increment[1].value) + 1);
		}.

		Decrement: .{
			int newVal = Math.max(0, $$SymbolTable.get($Decrement[1].value) - 1);
			$$SymbolTable.put($Decrement[1].value, newVal);
		}.

		Goto: .{
			if ($$SymbolTable.get($Goto[1].value) == 0)
				$ctx.derive($$ProgramTable.get($Goto[2].value));
		}.

		Reg: .{
			$Reg[0].value = $Reg[1].value;
			$ctx.addVariable(new VariableAccessor($Reg[1].value));
		}.

		Int: .{
			$Int[0].value = #0.text;
		}.
	}
}

endemic slice ram.SymbolTableEndemic {
	declare {
		SymbolTable .{ ram.SymbolTable.getInstance() }.
	}
}

endemic slice ram.ProgramTableEndemic {
	declare {
		ProgramTable .{ ram.ProgramTable.getInstance() }.
	}
}

language ram.RamLang {
	slices 
		ram.RamProgram
	endemic slices
		ram.SymbolTableEndemic
		ram.ProgramTableEndemic

	roles syntax < preevaluation < debug < evaluation	
}
