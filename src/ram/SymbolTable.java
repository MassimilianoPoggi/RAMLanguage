package ram;

import java.util.HashMap;
import java.util.Map;

public class SymbolTable {
	private static class Holder {
		static SymbolTable table = new SymbolTable();
	}
	
	private final Map<Integer, Integer> map = new HashMap<Integer, Integer>();
	
	public void put(Integer name, Integer value) {
		map.put(name, value);
	}
	
	public int get(Integer name) {
		if (!map.containsKey(name))
			map.put(name, 0);
		return map.get(name);
	}
	
	public static SymbolTable getInstance() {
		return Holder.table;
	}
}
