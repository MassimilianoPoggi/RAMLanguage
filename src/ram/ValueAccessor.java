package ram;

import java.util.ArrayList;
import java.util.List;

public class ValueAccessor implements neverlang.runtime.debug.variables.ValueAccessor {

	private final Integer register;

	public ValueAccessor(Integer register) {
		this.register = register;
	}

	@Override
	public String type() {
		return "integer";
	}

	@Override
	public String value() {
		return String.valueOf(SymbolTable.getInstance().get(register));
	}

	@Override
	public List<? extends neverlang.runtime.debug.variables.VariableAccessor> nestedVars() {
		return new ArrayList<>();
	}
}
