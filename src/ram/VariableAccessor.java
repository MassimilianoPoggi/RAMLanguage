package ram;

class VariableAccessor implements neverlang.runtime.debug.variables.VariableAccessor {
	private final Integer register;

	public VariableAccessor(Integer register) {
		this.register = register;
	}

	@Override
	public String name() {
		return "R" + register;
	}

	@Override
	public String type() {
		return "register";
	}

	@Override
	public neverlang.runtime.debug.variables.ValueAccessor value() {
		return new ValueAccessor(register);
	}
}
	
