package ram;

import java.util.HashMap;
import java.util.Map;
import neverlang.runtime.dexter.ASTNode;

public class ProgramTable {
	private static class Holder {
		static SymbolTable table = new SymbolTable();
	}
	
	private final Map<Integer, ASTNode> map = new HashMap<Integer, ASTNode>();
	
	public void put(Integer line, ASTNode instruction) {
		map.put(line, instruction);
	}
	
	public ASTNode get(Integer line) {
		return map.get(line);
	}
	
	public static SymbolTable getInstance() {
		return Holder.table;
	}
}
